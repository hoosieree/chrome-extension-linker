'use strict';
/* Activate extension on pages containing only a single PRE tag. */
if(document.body.children.length===1 && document.body.firstElementChild.tagName==='PRE'){
  const pres = document.getElementsByTagName('pre'),
        div0 = document.createElement('div'),
        rx0 = /(https?:[^"\s]*)/g,
        rx1 = '<a href="$1">$1<\/a>',
        css0 = 'style',
        css1 = 'font-family: monospace; white-space: pre-wrap;',
        dx=(pr)=>{
          let dv=div0.cloneNode(false);
          dv.innerHTML = String(pr.innerHTML).replace(rx0,rx1);
          dv.setAttribute(css0,css1);
          pr.parentNode.insertBefore(dv,pr);
          pr.parentNode.removeChild(pr);
        };

  /* procedure credit: http://stackoverflow.com/a/4005685/2037637 */
  let i=pres.length; while(i-->0){dx(pres[i]);}
}
